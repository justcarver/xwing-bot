// Using dotenv for the token information
require('dotenv').config();

// Imported Libraries
const Discord = require('discord.js');
const fs = require('fs');

// Create bot
const bot = new Discord.Client({disableEveryone: true});

// Bot options
const defaultPrefix = process.env.BOT_PREFIX;

// Setting up the Bot Commands
bot.commands = new Discord.Collection();

// Read Commands Directory and import each command
fs.readdir('./commands/', (err, files) => {
  // Check for errors and make sure we find files to import.
  if (err) console.log(err);
  let jsFile = files.filter(f => f.split('.').pop().toLowerCase() === 'js');
  if (jsFile.length <= 0) {
    console.log("Couldn't find commands.");
    return;
  }

  // Load each command file
  jsFile.forEach((f, i) => {
    let props = require(`./commands/${f}`);
    console.log(`${f} loaded!`);
    bot.commands.set(props.help.name, props);
  });
});

// Notify us the bot is ready, and set basic activity
bot.on("ready", async () => {
  console.log(`${bot.user.username} is online on ${bot.guilds.size} servers!`);
  bot.user.setActivity("X-Wing TMG Rules", {type: "PLAYING"});
});

bot.on("message", async message => {
  if (message.author.bot) return;
  if (message.channel.type === "dm") return;

  let prefixes = JSON.parse(fs.readFileSync("./data/prefixes.json", "utf8"));

  if (!prefixes[message.guild.id]) {
    prefixes[message.guild.id] = {
      prefix: defaultPrefix
    }
  }

  let prefix = prefixes[message.guild.id].prefix;

  let messageArray = message.content.split(' ');
  let cmd = messageArray[0];
  let args = messageArray.slice(1);
  if (cmd.startsWith(prefix)) {
    let commandFile = bot.commands.get(cmd.slice(prefix.length));
    if (commandFile) commandFile.run(bot, message, args);
  }
});

// Login the bot
bot.login(process.env.BOT_TOKEN);

