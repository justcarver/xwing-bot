const Discord = require('discord.js');
const camelCase = require('camelcase');
const actions = require('../data/actions.json');

module.exports.run = async (bot, message, args) => {
  let botIcon = bot.user.displayAvatarURL;
  let botEmbade = '';

  // if (message.channel.name != 'rules_questions') return;
  if (args.length > 0) {
    let action = camelCase(args).replace(/[.,\/#!$%\^&\*;:{}=\-_`~()\"]/g,"");
    if (actions.hasOwnProperty(action)) {
      botEmbed = new Discord.RichEmbed()
	      .setDescription("Actions")
	      .addField(actions[action].name,actions[action].text);
    } else {
      botEmbed = new Discord.RichEmbed()
	      .setDescription('Could not find action.')
	      .addField('Valid Action List',actions.actions.join(', '));
    }
  } else {
    botEmbed = new Discord.RichEmbed()
	    .setDescription('Actions')
	    .addField('Action List',actions.actions.join(', '));
  }
  message.channel.send(botEmbed);
}

module.exports.help = {
  name: "actions"
}
