// Using dotenv for the token information
require('dotenv').config();

// Bot options
const Discord = require("discord.js");
const fs = require("fs");

const defaultPrefix = process.env.BOT_PREFIX;

module.exports.run = async (bot, message, args) => {
  if (!message.member.hasPermission("MANAGE_SERVER")) return message.reply("You can not change the prefix");

  let prefixes = JSON.parse(fs.readFileSync("./data/prefixes.json", "utf8"));

  if (!prefixes[message.guild.id]) {
    prefixes[message.guild.id] = {
      prefix : defaultPrefix
    }
  }

  let prefix = prefixes[message.guild.id].prefix;

  if (!args[0] || args[0] == "help") return message.reply(`Usage: ${prefix}prefix <insert desired prefix here>`);

  prefixes[message.guild.id] = {
    prefix : args[0]
  }

  fs.writeFile("./data/prefixes.json", JSON.stringify(prefixes), (err) => {
    if (err) console.log(err);
  });

  let prefixEmbed = new Discord.RichEmbed()
    .setColor("#FF9900")
    .setTitle("Prefex Set")
    .setDescription(`Set to ${args[0]}`);

  message.channel.send(prefixEmbed);
  
}

module.exports.help = {
  name: "prefix"
}
