const Discord = require('discord.js');

module.exports.run = async (bot, message, args) => {
  let botIcon = bot.user.displayAvatarURL;
  let botEmbed = new Discord.RichEmbed()
	  .setDescription('Hello')
	  .addField('My Name', bot.user.username)
	  .addField('Created On', bot.user.createdAt);
  message.channel.send(botEmbed);
}

module.exports.help = {
  name: "hello"
}
