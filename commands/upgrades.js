// Using dotenv for the token information
require('dotenv').config();

const Discord = require('discord.js');
const mongoose = require('mongoose');
const mongoURL = process.env.MONGO_URL;
const config = {
  useNewUrlParser: true,
};

mongoose.Promise = global.Promise;
mongoose.connect(mongoURL, config);
let upgradeSchema = new mongoose.Schema({
  title: String,
  type: String,
  faction: String,
  limited: Number,
  hyperspace: Boolean,
  cost: String,
  imgLink: String
});

let Upgrades = mongoose.model("Upgrades", upgradeSchema);

module.exports.run = async (bot, message, args) => {
  let botIcon = bot.user.displayAvatarURL;
  let botEmbade = '';

  let db = mongoose.connection;

  db.on('error', console.error.bind(console, "MongoDB connection error:"));

  // if (message.channel.name != 'rules_questions') return;
  if (args.length > 0) {
    let upgrade = args.join(' ');

    Upgrades.find({ title: new RegExp(upgrade, 'i')}, function (err, docs) {
      if (docs.length == 0) {
	botEmbed = new Discord.RichEmbed()
		.setTitle("Upgrades")
		.setDescription("Unable to find any Upgrades named: " + upgrade);
	message.channel.send(botEmbed);
      } else if (docs.length == 1) {
	botEmbed = new Discord.RichEmbed()
		.setTitle(docs[0].title + ' (' + docs[0].type + ')')
		.addField("Cost", docs[0].cost)
		.addField("Faction", docs[0].faction)
		.addField("Hyperspace", docs[0].hyperspace)
		.setImage(docs[0].imgLink)
	message.channel.send(botEmbed);
      } else {
	docs.forEach(function (element) {
	  botEmbed = new Discord.RichEmbed()
		  .setTitle(element.title + ' (' + element.type + ')')
		  .addField("Cost", element.cost)
		  .addField("Faction", element.faction)
		  .addField("Hyperspace", element.hyperspace)
		  .setImage(element.imgLink)
	  message.channel.send(botEmbed);
	});
      }
    });
  }
}

module.exports.help = {
  name: "upgrades"
}

