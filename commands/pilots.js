// Using dotenv for the token information
require('dotenv').config();

const Discord = require('discord.js');
const mongoose = require('mongoose');
const mongoURL = process.env.MONGO_URL;
const config = {
  useNewUrlParser: true,
};

mongoose.Promise = global.Promise;
mongoose.connect(mongoURL, config);
let pilotSchema = new mongoose.Schema({
  faction: String,
  ship: String,
  name: String,
  limited: Number,
  hyperspace: Boolean,
  cost: Number,
  upgrades: String,
  imgLink: String
});

let Pilots = mongoose.model("Pilots", pilotSchema);

module.exports.run = async (bot, message, args) => {
  let botIcon = bot.user.displayAvatarURL;
  let botEmbade = '';

  let db = mongoose.connection;

  db.on('error', console.error.bind(console, "MongoDB connection error:"));

  // if (message.channel.name != 'rules_questions') return;
  if (args.length > 0) {
    if (args[0] == 'help') {
	message.channel.send("Command Help: ?pilots {name} | {faction}");
      return;
    }
    let separatorIndex = args.indexOf('|');
    let faction = "";
    let name = "";
    if (separatorIndex > 0) {
      faction = args.slice(separatorIndex + 1).join(' '); 
      name = args.slice(0, separatorIndex).join(' ');
    } else {
      name = args.slice(separatorIndex + 1).join(' ')
    }

    let query ={
      name: new RegExp(name, 'i')
    }
    if (faction) {
      query.faction = new RegExp(faction, 'i');
    }

    Pilots.find(query, function (err, docs) {
      if (docs.length == 0) {
	botEmbed = new Discord.RichEmbed()
		.setTitle("Pilots")
		.setDescription("Unable to find any Pilots named: " + name);
	message.channel.send(botEmbed);
      } else if (docs.length == 1) {
	botEmbed = new Discord.RichEmbed()
		.setTitle(docs[0].name+ ' (' + docs[0].ship+ ')')
		.addField(`Faction`, docs[0].faction)
		.addField(`Hyperspace`, docs[0].hyperspace)
		.addField(`Upgrades`, docs[0].upgrades)
		.addField(`Cost`, docs[0].cost)
		.setImage(docs[0].imgLink)
	message.channel.send(botEmbed);
      } else {
	docs.forEach(function (element) {
	  botEmbed = new Discord.RichEmbed()
		  .setTitle(element.name + ' (' + element.ship + ')')
		  .addField(`Faction`, element.faction)
		  .addField(`Hyperspace`, element.hyperspace)
		  .addField(`Upgrades`, element.upgrades)
		  .addField(`Cost`, element.cost)
		  .setImage(element.imgLink)
	  message.channel.send(botEmbed);
	});
      }
    });
  }
}

module.exports.help = {
  name: "pilots"
}

