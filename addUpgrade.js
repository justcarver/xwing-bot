// Using dotenv for the token information
require('dotenv').config();

const csvParse = require('csv-parse');
const fs = require('fs');
const mongoose = require('mongoose');
const mongoURL = process.env.MONGO_URL;
const config = {
  useNewUrlParser: true,
};

//const MongoClient = require('mongodb').MongoClient;

mongoose.Promise = global.Promise;

mongoose.connect(mongoURL, config);

let upgradeSchema = new mongoose.Schema({
  title: String,
  type: String,
  faction: String,
  limited: Number,
  hyperspace: Boolean,
  cost: String,
  imgLink: String
});

let Upgrades = mongoose.model("Upgrades", upgradeSchema);

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection error:"));

let csvArray = [];
const readFile = async () => {
  fs.createReadStream(`./data/Upgrades.csv`)
    .pipe(csvParse())
    .on('data', data => {
      csvArray.push(data);
    })
    .on('end', () => {
      processArray();
    });
}

const processArray = async () => {
  for ( i = 0; i < csvArray.length; i++) {
    let upgrade = csvArray[i];
    if (upgrade[4] == "Yes") {
      upgrade[4] = true;
    } else {
      upgrade[4] = false;
    }

    let newUpgrade = new Upgrades({
      title: upgrade[2],
      type: upgrade[1],
      faction: upgrade[0],
      limited: upgrade[3],
      hyperspace: upgrade[4],
      cost: upgrade[5],
      imgLink: upgrade[6]
    });

    console.log(newUpgrade);
    newUpgrade.save(function (err) {
      if (err) throw err;
      console.log('Upgrade "' + upgrade[2] + '" saved');
    });
  }
};

readFile();
