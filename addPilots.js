// Using dotenv for the token information
require('dotenv').config();

const csvParse = require('csv-parse');
const fs = require('fs');
const mongoose = require('mongoose');
const mongoURL = process.env.MONGO_URL;
const config = {
  useNewUrlParser: true,
};

//const MongoClient = require('mongodb').MongoClient;

mongoose.Promise = global.Promise;

mongoose.connect(mongoURL, config);

let pilotSchema = new mongoose.Schema({
  faction: String,
  ship: String,
  name: String,
  limited: Number,
  hyperspace: Boolean,
  cost: Number,
  upgrades: String,
  imgLink: String
});

let Pilot = mongoose.model("Pilots", pilotSchema);

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection error:"));

let csvArray = [];
const readFile = async () => {
  fs.createReadStream(`./data/Pilots.csv`)
    .pipe(csvParse())
    .on('data', data => {
      csvArray.push(data);
    })
    .on('end', () => {
      processArray();
    });
}

const processArray = async () => {
  for ( i = 0; i < csvArray.length; i++) {
    let pilot = csvArray[i];
    if (pilot[4] == "Yes") {
      pilot[4] = true;
    } else {
      pilot[4] = false;
    }

    let newPilot = new Pilot({
      faction: pilot[0],
      ship: pilot[1],
      name: pilot[2],
      limited: pilot[3],
      hyperspace: pilot[4],
      cost: pilot[5],
      upgrades: pilot[6],
      imgLink: pilot[7]
    });

    newPilot.save(function (err) {
      if (err) throw err;
      console.log('Upgrade "' +pilot[2] + '" saved');
    });
  }
};

readFile();
